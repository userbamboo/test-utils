#!/bin/bash -e

SIZE=${1:-8192}

rm -rf out/large
mkdir -p out && mkdir -p out/large
echo "Creating large files with size `echo $SIZE` bytes"
dd if=/dev/urandom of=out/large/sample.txt bs=$SIZE count=15


